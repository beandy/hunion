function openFile(e, whereToPutContent) {
	var files = e.target.files
	var file = files[0]
	var reader = new FileReader()
	reader.onload = function(e) {
		var fileName = file.name
		var fileExtension = fileName.split('.').pop()
		if(fileExtension === 'xlsx' || fileExtension ===  'xls' || fileExtension === 'csv' || fileExtension === 'ods') {
			handleSpreadsheetFile(e, file, reader, whereToPutContent)
		} else {
			var fileContent = e.target.result
			whereToPutContent.setValue(fileContent)
		}		
	};
	reader.readAsText(file)
}