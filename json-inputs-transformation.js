/* START : code editor */
const inputCodeMirror = CodeMirror(document.getElementById('input-area'), {
  mode:  {
  	name: "javascript",
  	json: true
  },
  theme: "ayu-mirage"
});

const templateCodeMirror = CodeMirror(document.getElementById('template-area'), {
  mode:  {
  	name: "javascript",
  	json: true
  },
  theme: "ayu-mirage"
});

const resultCodeMirror = CodeMirror(document.getElementById('result-area'), {
  mode:  {
  	name: "javascript",
  	json: true
  },
  theme: "ayu-mirage"
});
/* END : code editor */

var uploadButtons = document.getElementsByClassName('upload-spreadsheet-btn');
function addListenerToUploadSpreadsheetButtons() {
	for (b=0; b<uploadButtons.length; b++) {
		uploadButtons[b].addEventListener('change', (e) => {openFile(e, inputCodeMirror)}, false)
	}
}

/* START : Handling run of JSONPath expression */
var runTemplateButtons = document.getElementsByClassName('run-template-btn');
function addListenerToRunTemplateButtons() {
	for (b=0; b<runTemplateButtons.length; b++) {
		runTemplateButtons[b].addEventListener('click', runTemplate, false)
	}
}

function runTemplate() {
	removeAllToastAlerts()
	if(isValidJSON(templateCodeMirror.getValue())) {
		var input = JSON.parse(inputCodeMirror.getValue())
		var template = JSON.parse(templateCodeMirror.getValue())
		readTemplate(input, template)
	} else {
		popToastAlert("Your template does not respect JSON syntax properly.")
		console.error("Your template does not respect JSON syntax properly.")
	}
}

function readTemplate(input, template) {
	var templateKeys = Object.keys(template)
	for(k=0; k<templateKeys.length; k++) {
		if(typeof template[templateKeys[k]] == 'object') {
			var keys = Object.keys(template[templateKeys[k]])
			for(sk=0; sk<keys.length; sk++) {
				if(checkTemplateValueIsExpression(template[templateKeys[k]][keys[sk]])) {
					template[templateKeys[k]][keys[sk]] = runValueExpression(input, template[templateKeys[k]][keys[sk]].substring(5))
				}
			}
		}
		if(checkTemplateValueIsExpression(template[templateKeys[k]])) {
			template[templateKeys[k]] = runValueExpression(input, template[templateKeys[k]].substring(5))
		}
	}
	var result = JSON.stringify(template, undefined, 4)
	resultCodeMirror.setValue(result)
}

function runValueExpression(input, expr) {
	var value = jsonPath(input, expr, {resultType: "VALUE"})
	return value
}

function checkTemplateValueIsExpression(value) {
	return typeof value === 'string' && value.substring(0,5) === '!jsp:';
}

/* END : Handling run of JSONPath expression */

/* START : load sample values */
var loadDataSamplesLinks = document.getElementsByClassName('load-data-samples-link');
function addListenerToLoadDataSamplesLinks() {
	for (let b=0; b<loadDataSamplesLinks.length; b++) {
		loadDataSamplesLinks[b].addEventListener('click', () => {loadDataSamples([
			{inputAreaName:inputCodeMirror, inputSampleContent:originalInputSample},
			{inputAreaName:templateCodeMirror, inputSampleContent:templateSample},
		])}, false)
	}
}
/* END : load sample values */

/* START : Handling the save of a template or result blob */

var saveTemplateButtons = document.getElementsByClassName('save-template-btn');
function addListenerToSaveTemplateButtons() {
	for (b=0; b<saveTemplateButtons.length; b++) {
		saveTemplateButtons[b].addEventListener('click', () => {saveFile(templateCodeMirror)}, false)
	}
}

var saveResultButtons = document.getElementsByClassName('save-result-btn');
function addListenerToSaveResultButtons() {
	for (b=0; b<saveResultButtons.length; b++) {
		saveResultButtons[b].addEventListener('click', () => {saveFile(resultCodeMirror)}, false)
	}
}
/* END : Handling the save of a template or result blob */

/* START : Handle opening a template file */
var openTemplateButtons = document.getElementsByClassName('open-template-btn');
function addListenerToOpenTemplateButtons() {
	for (b=0; b<openTemplateButtons.length; b++) {
		openTemplateButtons[b].addEventListener('change', (e) => {openFile(e, templateCodeMirror)}, false)
	}
}

/* END : Handle opening a template file */

/* START : Check JSON validity syntax */
function isValidJSON(str) {
	try {
		JSON.parse(str);
	} catch(e) {
		return false;
	}
	return true;
}
/* END : Check JSON validity syntax */

window.onload = function() {
	addListenerToDropdowns();
	addListenerToUploadSpreadsheetButtons();
	addListenerToRunTemplateButtons();
	addListenerToSaveTemplateButtons();
	addListenerToSaveResultButtons();
	addListenerToOpenTemplateButtons();
	addListenerToLoadDataSamplesLinks();
};