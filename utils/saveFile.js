function saveFile(contentSource) {
	const a = document.createElement('a')
	var content = contentSource.getValue()
	var blob = new Blob([content], {type: 'application/json'})
	var name = 'Hunion_'+Date.now()+'.json'

	a.href = URL.createObjectURL(blob)
	a.download = name
	a.click()

	URL.revokeObjectURL(a.href)
}