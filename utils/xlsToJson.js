/* START : Settings */
var settings = {
	upload : {
		booleanCheckSensitiveToCase : false,
		considerStartAndEndWithBracketAsArray : false
	}
}
/* END : Settings */

/* START : Handling upload of file/workbook */
function handleSpreadsheetFile(e, file, reader, whereToPutContent) {
	reader.onload = function(e) {
		var data = new Uint8Array(e.target.result);
		var workbook = XLSX.read(data, {type: 'array'});
		/* DO SOMETHING WITH workbook HERE */
		try {
			checkInWorkbookDuplicateSheetNames(workbook)
			convertWorkbookToObject(workbook, whereToPutContent)
		} catch (e) {
			console.error(e)
		}
		
	};
	reader.readAsArrayBuffer(file);
}

function checkInWorkbookDuplicateSheetNames(workbook) {
	for (let s=0; s<workbook.SheetNames.length; s++) {
		if(workbook.SheetNames.slice(s+1).includes(workbook.SheetNames[s])) {
			throw "duplicate sheet's name : " + workbook.SheetNames[s]
		}
	}
}

const xlsDicCol = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"];

function countSheetColumns(sheet) {
	var separatorIndex = sheet["!ref"].indexOf(":");
	var firstColName = sheet["!ref"].substring(0, separatorIndex);
	var firstColNameAlpha = firstColName.replace(/[0-9]/g, '');
	var firstColIndex = 0;
	var lastColName = sheet["!ref"].substring(separatorIndex+1);
	var lastColNameAlpha = lastColName.replace(/[0-9]/g, '');
	var lastColIndex = 0;

	for (let c=0 ; c < lastColNameAlpha.length ; c++) {
		lastColIndex = lastColIndex + xlsDicCol.indexOf(lastColNameAlpha[c])
	}

	var rangeCol = 0;
	if(lastColNameAlpha.length > 1) {
		rangeCol =+ lastColIndex + (1 + xlsDicCol.length)
	} else {
		rangeCol =+ lastColIndex + 1
	}
	return rangeCol
}

function getSheetColumnsHeader(range, sheet) {
	var columnsHeader = [];

	var firstLetterIndex = -1;
	var c = 0
	for (let i=0 ; i < (range+1) ; i++) {
		if(c < xlsDicCol.length) {
			var name ;
			if(firstLetterIndex < 0) {
				name = xlsDicCol[c] + "1"
			} else {
				name = xlsDicCol[firstLetterIndex] + xlsDicCol[c] + "1"
			}
			columnsHeader.push(sheet[name]);
			c++
		} else {
			c=0;
			firstLetterIndex++
		}
	}
	return columnsHeader
}

function countSheetRows(sheet) {
	var separatorIndex = sheet["!ref"].indexOf(":");
	var lastCell = sheet["!ref"].substring(separatorIndex+1);
	var rowsCount = lastCell.replace(/\D/g,'');

	return rowsCount
}

function countSheetCells(sheet) {
	var countOfColumns = countSheetColumns(sheet)
	var countOfRows = countSheetRows(sheet)
	var cellsCount = countOfColumns * countOfRows

	return cellsCount
}
/* END : Handling upload of file/workbook */

/* START : convert workbook into a common structured JSON */
// 1 fichier = 1 objet, 1 feuille = 1 liste, 1 ligne = 1 objet, 1 colonne = 1 propriété d'objet
function convertWorkbookToObject(workbook, whereToPutContent) {
	var sheetItems = [];
	for (let s=0; s < workbook.SheetNames.length; s++) {
		var sheetName = workbook.SheetNames[s]
		var sheet = workbook.Sheets[sheetName]
		sheetItems.push(convertSheetToArray(sheetName, sheet))
	}

	var workbookLitteralObj = `{"Workbook":{${sheetItems}}}`
	workbookLitteralObj = workbookLitteralObj.replaceAll(',}', '}')
	var workbookObj = JSON.parse(workbookLitteralObj)
	var pretty = JSON.stringify(workbookObj, undefined, 4);
	whereToPutContent.setValue(pretty)
}

function convertSheetToArray(sheetName, sheet){
	var countOfColumns = countSheetColumns(sheet)
	var countOfRows = countSheetRows(sheet)
	var countOfCells = countSheetCells(sheet)
	var sheetHeaders = getSheetColumnsHeader(countOfColumns, sheet)

	function insertRowsAsObjects() {
		var items = [];
		for (let i=1; i < countOfRows; i++) {
			var item = `{${insertColumnsAsProperties(i)}}`
			items.push(item)
		}
		return items
	}

	function getCellValue(columnAlpha, rowNumeric) {
		var cellId = columnAlpha + rowNumeric.toString();
		var cellValue
		if (sheet.hasOwnProperty(cellId)) {
			cellValue = sheet[cellId].v
			if(checkValueIsBoolean(cellValue)) {
				cellValue = JSON.parse(cellValue.toLowerCase())
			} else if(checkValueIsNumber(cellValue)) {
				cellValue = parseFloat(cellValue)
			} else if(checkValueIsArray(cellValue)) {
				cellValue = JSON.parse(cellValue)
			}
		} else {
			cellValue = ""
		}

		return JSON.stringify(cellValue)		
	}
	
	function insertColumnsAsProperties(row) {
		var propertiesNames = ``;
		var rowNumeric = row + 1;
		for (let c=0; c < countOfColumns; c++) {
			if(sheetHeaders[c]) {
				function insertProperty(columnAlpha, rowNumeric) {
					if(c != countOfColumns-1) {
						propertiesNames += `${JSON.stringify(sheetHeaders[c].v)}:${getCellValue(columnAlpha, rowNumeric)},`
					} else {
						propertiesNames += `${JSON.stringify(sheetHeaders[c].v)}:${getCellValue(columnAlpha, rowNumeric)}`
					}
				}
				
				var columnAlpha
				var firstLetterIndex = -1

				if(c < xlsDicCol.length) {
					if(firstLetterIndex < 0) {
						columnAlpha = xlsDicCol[c];
						insertProperty(columnAlpha, rowNumeric)
					}
					else {
						columnAlpha = xlsDicCol[firstLetterIndex] + xlsDicCol[c];
						insertProperty(columnAlpha, rowNumeric)
					}
				} else {
					columnAlpha = xlsDicCol[firstLetterIndex] + xlsDicCol[c];
					insertProperty(columnAlpha, rowNumeric)
					firstLetterIndex++
				}
			}
		}
		return propertiesNames
	}

	var sheetLitteralObj = `${JSON.stringify(sheetName)}:[${insertRowsAsObjects()}]`
	return sheetLitteralObj
}

/* END : convert workbook into a common structured JSON */

/* START : functions checking kind of cell's value to transform it (or not) */

function checkValueIsNumber(value) {
	// on va considérer que si la chaîne a : 1 seule point (optionnel) et le reste des autres caractères ne sont que numérique ; alors c'est un number
	var isNumber
	var parts
	var numbers = []

	//Si le tableau 'parts' a plus de 2 elements, alors ça veut dire qu'il y a plus qu'un seul caractère '.' ; donc je considère que ce n'est pas un nombre
	if(typeof value == 'string') {
		parts = value.split('.')
		
		if(parts.length<3) {
			for (let p=0; p<parts.length; p++) {
				numbers.push(Number(parts[p]))
			}
			// Après conversion, si il y a au moins un element qui n'est pas un number, alors je considère que l'ensemble de la chaîne n'est pas un nombre
			if(numbers.find(e => Number.isNaN(e)) == undefined) {
				isNumber = true
			} else {
				isNumber = false
			}
		} else {
			isNumber = false
		}
	}
	
	return isNumber	
}

function checkValueIsBoolean(value) {
	var isBoolean

	if(!settings.upload.booleanCheckSensitiveToCase && typeof value == 'string') {
		value = value.toLowerCase()
	}

	if(value === 'true' || value === 'false') {
		isBoolean = true
	} else {
		isBoolean = false
	}
	return isBoolean
}

function checkValueIsArray(value) {
	var isArray

	if(!settings.upload.considerStartAndEndWithBracketAsArray && typeof value == 'string') {
		if(value[0] == '[' && value[(value.length)-1] == ']') {
			isArray = true
		} else {
			isArray = false
		}
	}
	return isArray
}

/* END : functions checking kind of cell's value to transform it (or not) */