/* START : code editor */
const inputArea = CodeMirror(document.getElementById('input-area'), {
  mode:  {
  	name: "javascript",
  	json: true
  },
  theme: "ayu-mirage"
});

const resultArea = CodeMirror(document.getElementById('result-area'), {
  mode:  {
  	name: "javascript",
  	json: true
  },
  theme: "ayu-mirage"
});
/* END : code editor */

var loadDataSamplesLinks = document.getElementsByClassName('load-data-samples-link');
function addListenerToLoadDataSamplesLinks() {
	for (let b=0; b<loadDataSamplesLinks.length; b++) {
		loadDataSamplesLinks[b].addEventListener('click', () => {loadDataSamples([
			{inputAreaName:inputArea, inputSampleContent:originalInputSample}
		])}, false)
	}
}

var openInputButtons = document.getElementsByClassName('upload-input-btn');
function addListenerToOpenInputButtons() {
	for (let b=0; b<openInputButtons.length; b++) {
		openInputButtons[b].addEventListener('change', (e) => {openFile(e, inputArea)}, false)
	}
}

var saveResultButtons = document.getElementsByClassName('save-result-btn');
function addListenerToSaveResultButtons() {
	for (let b=0; b<saveResultButtons.length; b++) {
		saveResultButtons[b].addEventListener('click', () => {saveFile(resultArea)}, false)
	}
}

function listObjectKeys(object) {
	const objectKeys = Object.keys(object)

	for(let k=0; k<objectKeys.length; k++) {
		const key = objectKeys[k]
		const value = object[key]

		if(typeof value !== 'object') {
			object[objectKeys[k]] = typeof value
		}

		else if(typeof value === 'object') {
			if(Array.isArray(value)) {
				for(let ik=0; ik<value.length; ik++) {
					listObjectKeys(value[ik])
				}
			}
			else if (value !== null) {
				listObjectKeys(value)
			}
		}
	}

	var result = JSON.stringify(object, undefined, 4)
	resultArea.setValue(result) 
}

function parseInputArea(inputArea) {
	return JSON.parse(inputArea.getValue())
}

document.getElementById('get-keys-btn-top').addEventListener('click', () => {listObjectKeys(parseInputArea(inputArea))}, false)

window.onload = function() {
	addListenerToDropdowns();
	addListenerToOpenInputButtons();
	addListenerToSaveResultButtons();
	addListenerToLoadDataSamplesLinks()
};