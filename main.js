/* START : All about dynamic styles */
function hideElement(element) {
	document.getElementById(element).classList.add('is-hidden');
}
function showElement(element) {
	document.getElementById(element).classList.remove('is-hidden');
}

function toggleDropdown() {
	this.classList.toggle('is-active')
}
var dropdowns = document.getElementsByClassName('dropdown');
function addListenerToDropdowns() {
	for (let d=0; d<dropdowns.length; d++) {
		dropdowns[d].addEventListener('click', toggleDropdown, false)
	}
}
/* END : All about dynamic styles */

/* START : All about alerts */
const templateAlertsBox = document.getElementById('toast-alerts')

function popToastAlert(message) {
	var alert = document.createElement('article');
	alert.classList.add('message', 'is-danger', 'toast-alert');
	alert.innerHTML = `<div class="message-body">${message}</div>`;
	templateAlertsBox.appendChild(alert);
	window.scrollTo(0,0)
}

function removeAllToastAlerts() {
	while(templateAlertsBox.firstChild) {
		templateAlertsBox.removeChild(templateAlertsBox.firstChild)
	}
}
/* END : All about alerts */

/* START : Check JSON validity syntax */
function isValidJSON(str) {
	try {
		JSON.parse(str);
	} catch(e) {
		return false;
	}
	return true;
}
/* END : Check JSON validity syntax */