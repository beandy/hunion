# Hunion

Grants quick tools to insert and manipulate JSON values through interfaces.
Like for example : 
* **JSON inputs transformation** : Write template in JSON (with ability to insert in it [JSONPath](https://goessner.net/articles/JsonPath/) expressions). Ta-dam get as output a JSON following your template, and filled with your inputs values.
* **JSON merge prefixed values** : Pass a JSON where you want to replace all prefixed values with other values from another JSON input.
* **JSON get keys** : Pass a JSON as an input, and obtain the list of all the keys (plus their type).

More to come (maybe...)
