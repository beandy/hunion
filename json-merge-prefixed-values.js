/* START : code editor */
const originalInputArea = CodeMirror(document.getElementById('original-input-area'), {
  mode:  {
  	name: "javascript",
  	json: true
  },
  theme: "ayu-mirage"
});

const sourceInputArea = CodeMirror(document.getElementById('source-input-area'), {
  mode:  {
  	name: "javascript",
  	json: true
  },
  theme: "ayu-mirage"
});

const resultArea = CodeMirror(document.getElementById('result-area'), {
  mode:  {
  	name: "javascript",
  	json: true
  },
  theme: "ayu-mirage"
});
/* END : code editor */

var prefix = ""
var sourceOrigin = sourceInputArea
var sourceKeyOrigin = ""

/* START : parameters panel */
const applyParametersButton = document.getElementById('apply-parameters-btn')
const prefixInput = document.getElementById('prefix-input')
const sourceKeyOriginInput = document.getElementById('source-key-origin-input')

function applyParameters() {
	console.log(prefix)
	prefix = prefixInput.value
	console.log(prefix)
	sourceKeyOrigin = sourceKeyOriginInput.value
}

applyParametersButton.addEventListener('click', applyParameters)
/* END : parameters panel */

/* START : load sample values */
var loadDataSamplesLinks = document.getElementsByClassName('load-data-samples-link');
function addListenerToLoadDataSamplesLinks() {
	for (let b=0; b<loadDataSamplesLinks.length; b++) {
		loadDataSamplesLinks[b].addEventListener('click', () => {loadDataSamples([
			{inputAreaName:originalInputArea, inputSampleContent:originalInputSample},
			{inputAreaName:sourceInputArea, inputSampleContent:sourceOfValuesSample},
		])}, false)
	}
}
/* END : load sample values */

var openOriginalInputButtons = document.getElementsByClassName('upload-original-input-btn');
function addListenerToOpenOriginalInputButtons() {
	for (let b=0; b<openOriginalInputButtons.length; b++) {
		openOriginalInputButtons[b].addEventListener('change', (e) => {openFile(e, originalInputArea)}, false)
	}
}

var openSourceInputButtons = document.getElementsByClassName('upload-source-input-btn');
function addListenerToOpenSourceInputButtons() {
	for (let b=0; b<openSourceInputButtons.length; b++) {
		openSourceInputButtons[b].addEventListener('change', (e) => {openFile(e, sourceInputArea)}, false)
	}
}

var saveResultButtons = document.getElementsByClassName('save-result-btn');
function addListenerToSaveResultButtons() {
	for (let b=0; b<saveResultButtons.length; b++) {
		saveResultButtons[b].addEventListener('click', () => {saveFile(resultArea)}, false)
	}
}

function parseObject(object) {
	const objectKeys = Object.keys(object)

	for(let k=0; k<objectKeys.length; k++) {
		const key = objectKeys[k]
		const value = object[key]
		if(typeof value === 'string') {
			const valuePrefix = value.substring(0, prefix.length)
			if(valuePrefix === prefix) {
				replacePrefixedValue(object, key, value.substring(prefix.length), sourceOrigin, sourceKeyOrigin)
			}
		}
		else if(typeof value === 'object') {
			if(Array.isArray(value)) {
				for(let ik=0; ik<value.length; ik++) {
					parseObject(value[ik])
				}
			}
			else if(value !== null) {
				parseObject(value)
			}
		}
	}
	resultArea.setValue(JSON.stringify(object, null, 4))
}

function getDesiredValue(value, sourceOrigin, sourceKeyOrigin) {
	const source = parseInputArea(sourceOrigin)
	const desiredValue = source[sourceKeyOrigin][value]

	return desiredValue
}

function replacePrefixedValue(object, cursor, value, sourceOrigin, sourceKeyOrigin) {
	const desiredValue = getDesiredValue(value, sourceOrigin, sourceKeyOrigin)
	object[cursor] = desiredValue
}

function parseInputArea(inputArea) {
	return JSON.parse(inputArea.getValue())
}

document.getElementById('merge-json-btn-top').addEventListener('click', () => {
	removeAllToastAlerts()
	if(sourceKeyOrigin == "" || prefix == "") {
		popToastAlert("A prefix and/or the key to fetch the values, are missing. Please insert parameters inputs and apply.")
		console.error("A prefix and/or the key to fetch the values, are missing. Please insert parameters inputs and apply.")
	} else {
		parseObject(parseInputArea(originalInputArea))
	}
}, false)

window.onload = function() {
	addListenerToDropdowns();
	addListenerToOpenOriginalInputButtons();
	addListenerToOpenSourceInputButtons();
	addListenerToSaveResultButtons();
	addListenerToLoadDataSamplesLinks();
};